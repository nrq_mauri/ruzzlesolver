public class Letter {
	
	private int position;
	private char letter;
	private int[] neighborhood;

	
	public Letter(int position){
		this.position=position;
		
	}

	public Letter(int position,char letter,int[] neighborhood) {
		this.position=position;
		this.letter=letter;
		this.neighborhood=neighborhood;
	}
	
	public void setPosition(int position){
		this.position=position;
	}
	public void setLetter(char letter)
	{
		this.letter=letter;
	}
	public int getPosition()
	{
		return this.position;
	}
	public char getLetter()
	{
		return this.letter;
	}
	public int[] getNeighborhood()
	{
		return neighborhood;
	}
	public void setNeighborhood(int[] neighborhood)
	{
		this.neighborhood=neighborhood;
	}
	/**
	 * 
	 * @param letter
	 * @return true se la lettera e uguale a letter
	 */
	public boolean isLetter(char letter)
	{
		return this.letter==letter ? true:false;
	}
	/**
	 * Controlla se la prima lettera della parola word corrisponde alla lettera assegnata a questa classe Letter
	 * Poi cerca nei sui vicini se la seguente lettera di word � presente tra di loro
	 * @param word la parola da cercare
	 * @param output la Word da creare con le lettere trovate
	 * @return true se tutta la parola word � stata trovata
	 */
	public boolean find(int commingFrom,String word,Word output)
	{
		if(isLetter(word.charAt(0)))
		{
			
			if(word.length()>1){
				for(int i=0;i<neighborhood.length;i++){
					if(neighborhood[i]!=commingFrom){
						if(Matrix.letters.get(neighborhood[i]).find(position,word.substring(1), output)){
							output.addLetter(this);
							return true;
						}
					}
						
				}
			}else{
				output.addLetter(this);
				return true;
			}
		}
		return false;
	}



}
