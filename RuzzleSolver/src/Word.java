import java.util.ArrayList;


public class Word {
	
	public ArrayList<Letter> letters;
	
	public Word()
	{
		this.letters=new ArrayList<Letter>();
	}

	public void addLetter(Letter letter){
		letters.add(letter);
	}
	public void clean(){
		letters.clear();
	}
	public String getWordAsString()
	{
		StringBuilder output = new StringBuilder();
		for(Letter letter:letters){
			output.append(letter.getLetter());
			
		}
		return output.reverse().toString();
	}
	
	//TODO creare un metodo per reperire il percorso delle lettere della parola corrente
	/*
	 * Usare il metodo Letter.getPosition()
	 */
}
