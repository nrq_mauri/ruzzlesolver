import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;


public class Launcher {

	private static ArrayList<String> db = new ArrayList<String>();

	private static Matrix matrix;

	private static ArrayList<Word> matches;

	/**
	 * @param args
	 * @throws IOException 
	 */
	public static void main(String[] args) throws IOException {

		matrix = new Matrix();
		
		matches = new ArrayList<Word>();

		/*
		 * caricamento dizionario
		 * 
		 */
		BufferedReader br = new BufferedReader(new FileReader("src/pa.txt"));
		 StringBuilder sb = new StringBuilder();
	        String line = br.readLine();
	        while (line != null) {
	            sb.append(line);
	            sb.append(System.lineSeparator());
	            line = br.readLine();
	            db.add(line);
	        }
	       // String everything = sb.toString();
		////////////////////////////////////////////////////
		
		for(int i=0;i<16;i++)
		{
			System.out.println("Enter something here : ");

			try{
				BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in));
				String s = bufferRead.readLine();
				addLetter(i,s.charAt(0));

			}
			catch(IOException e)
			{
				e.printStackTrace();
			}

		}
		//TODO implementazione uso dizzionario Italiano
		/*
		 * Caricare il dizionario italiano e per ogni parola cercare se � dentro la matrix
		 * Usando il metodo Matrix.find(String,Word)
		 * 
		 */
		for(String word : db){
			if (word==null)
				break;
			//System.out.println("\nsono qui");
			Word output = new Word();
			if(matrix.find(word.toString(), output))
				matches.add(output);		
		}
		
		System.out.println("****** PRINTING WORDS MATCHES ********");
		for(Word word : matches){
			System.out.println(word.getWordAsString());
		}
		
		//TODO
		/*
		 * Aggiungere System.out.println del percorso usato per trovare ogni parola,
		 * usando il metedo implementato in Word
		 * 
		 */

	}

	private static void addLetter(int position,char letter)
	{
		switch (position) {
		case Matrix.M11:
			matrix.addLetter(position, 
					new Letter(
							position, 
							letter, 
							new int[]{Matrix.M12,Matrix.M21,Matrix.M22}
							));
			break;
		case Matrix.M12:
			matrix.addLetter(position, 
					new Letter(
							position, 
							letter, 
							new int[]{Matrix.M11,Matrix.M13,Matrix.M21,Matrix.M22,Matrix.M23}
							));
			break;
		case Matrix.M13:
			matrix.addLetter(position, 
					new Letter(
							position, 
							letter, 
							new int[]{Matrix.M12,Matrix.M14,Matrix.M22,Matrix.M23,Matrix.M24}
							));
			break;
		case Matrix.M14:
			matrix.addLetter(position, 
					new Letter(
							position, 
							letter, 
							new int[]{Matrix.M13,Matrix.M23,Matrix.M24}
							));
			break;
		case Matrix.M21:
			matrix.addLetter(position, 
					new Letter(
							position, 
							letter, 
							new int[]{Matrix.M11,Matrix.M12,Matrix.M22,Matrix.M31,Matrix.M32}
							));
			break;
		case Matrix.M22:
			matrix.addLetter(position, 
					new Letter(
							position, 
							letter, 
							new int[]{Matrix.M11,Matrix.M12,Matrix.M13,Matrix.M21,Matrix.M23,Matrix.M31,Matrix.M32,Matrix.M33}
							));
			break;
		case Matrix.M23:
			matrix.addLetter(position, 
					new Letter(
							position, 
							letter, 
							new int[]{Matrix.M12,Matrix.M13,Matrix.M14,Matrix.M22,Matrix.M24,Matrix.M32,Matrix.M33,Matrix.M34}
							));
			break;
		case Matrix.M24:
			matrix.addLetter(position, 
					new Letter(
							position, 
							letter, 
							new int[]{Matrix.M13,Matrix.M14,Matrix.M23,Matrix.M33,Matrix.M34}
							));
			break;
		case Matrix.M31:
			matrix.addLetter(position, 
					new Letter(
							position, 
							letter, 
							new int[]{Matrix.M21,Matrix.M22,Matrix.M32,Matrix.M41,Matrix.M42}
							));
			break;
		case Matrix.M32:
			matrix.addLetter(position, 
					new Letter(
							position, 
							letter, 
							new int[]{Matrix.M21,Matrix.M22,Matrix.M23,Matrix.M31,Matrix.M33,Matrix.M41,Matrix.M42,Matrix.M43}
							));
			break;
		case Matrix.M33:
			matrix.addLetter(position, 
					new Letter(
							position, 
							letter, 
							new int[]{Matrix.M22,Matrix.M23,Matrix.M24,Matrix.M32,Matrix.M34,Matrix.M42,Matrix.M43,Matrix.M44}
							));
			break;
		case Matrix.M34:
			matrix.addLetter(position, 
					new Letter(
							position, 
							letter, 
							new int[]{Matrix.M23,Matrix.M24,Matrix.M33,Matrix.M43,Matrix.M44}
							));
			break;
		case Matrix.M41:
			matrix.addLetter(position, 
					new Letter(
							position, 
							letter, 
							new int[]{Matrix.M31,Matrix.M32,Matrix.M42}
							));
			break;
		case Matrix.M42:
			matrix.addLetter(position, 
					new Letter(
							position, 
							letter, 
							new int[]{Matrix.M31,Matrix.M32,Matrix.M33,Matrix.M41,Matrix.M43}
							));
			break;
		case Matrix.M43:
			matrix.addLetter(position, 
					new Letter(
							position, 
							letter, 
							new int[]{Matrix.M32,Matrix.M33,Matrix.M34,Matrix.M42,Matrix.M44}
							));
			break;
		case Matrix.M44:
			matrix.addLetter(position, 
					new Letter(
							position, 
							letter, 
							new int[]{Matrix.M33,Matrix.M34,Matrix.M43}
							));
			break;

		default:
			break;
		}
	}

}
