import java.util.ArrayList;
import java.util.List;


public class Matrix {
	
	public static final int M11 = 0;
	public static final int M12 = 1;
	public static final int M13 = 2;
	public static final int M14 = 3;
	public static final int M21 = 4;
	public static final int M22 = 5;
	public static final int M23 = 6;
	public static final int M24 = 7;
	public static final int M31 = 8;
	public static final int M32 = 9;
	public static final int M33 = 10;
	public static final int M34 = 11;
	public static final int M41 = 12;
	public static final int M42 = 13;
	public static final int M43 = 14;
	public static final int M44 = 15;
	
	public static List<Letter> letters;
	public int[] positions;
	public String word;
	public int currentChar=0;
	
	public Matrix() {
		letters = new ArrayList<Letter>();
	}
	
	public void addLetter(int position,Letter letter)
	{
		letters.add(position,letter);
	}
	
	public boolean contains(String word)
	{
		this.word=word;
		positions = new int[word.length()];
		return true;
	}
	/***
	 * Cerca la parola nella matrix, se la trova, salva la parola in una lista.
	 * @param word la parola da cercare
	 * @param output la classe Word dove salvare la parola
	 * @return true se trova la parola
	 */
	public boolean find(String word,Word output)
	{
		for(Letter letter:letters)
		{
			if(letter.find(letter.getPosition(),word.toString(), output))
				return true;
		}
		
		return false;
	}


}
